# How to run
## Requirements:
- node (tested on 8.12.0)
- yarn 
## Running:
Clone this repository, then:
- yarn install
- yarn start

1)  Open Binary Ninja
2)  Enable Plugin 'Binja Start/Stop XML Server'

3)  Open Admin PowerShell window
3a) Navigate to:  cd C:\Users\Chris\Documents\Work\PhD\JavaScript\Binja-NodeJS
3b) node .\fileupdate.js

4)  Open Powershell window
4a) cd C:\Users\Chris\Documents\Work\PhD\JavaScript\Binja-complex-HTML
4b) yarn start

5)  Open Chrome
5a) Navigate to: http://localhost:8080/

